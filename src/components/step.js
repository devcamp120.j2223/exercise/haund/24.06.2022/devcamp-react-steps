import { Component } from "react";

class Step extends Component {
    render() {
        const {id, title, content } = this.props

        return(
            <div>
                Bước {id}.{title} : {content}
            </div>
        )
    }
}

export default Step;